package com.swaglabs.stepdefinitions;

import com.github.javafaker.Faker;
import com.swaglabs.pages.LoginPage;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.support.PageFactory;

import static com.swaglabs.stepdefinitions.Hooks.driver;

public class StepWrongCredentials {

    //LoginPage loginPage;

    @Given("I want to login in webpage named saucedemo")
    public void iWantToLoginInWebpageNamedSaucedemo() {

        driver.get("https://www.saucedemo.com/");

    }

    @When("I enters an invalid user and password")
    public void iEntersAnInvalidUserAndPassword() {

        Faker faker = new Faker();

        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        loginPage.sendInfoUserName(faker.name().firstName());
        loginPage.sendIfoPassword(faker.name().lastName());
        loginPage.clickBtnLogin();
    }

    @Then("Its appear message of username and password are invalid")
    public void itsAppearMessageOfUsernameAndPasswordAreInvalid() {

        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        Assert.assertEquals("Epic sadface: Username and password do not match any user in this service", loginPage.validateMessageInvalidUser());
    }
}
