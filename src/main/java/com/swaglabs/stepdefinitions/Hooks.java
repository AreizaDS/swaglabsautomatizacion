package com.swaglabs.stepdefinitions;

import com.swaglabs.log.Log4j;
import io.cucumber.java.Before;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Hooks {

    public static WebDriver driver;
    public static Log4j log4j;

    @Before
    public void setUp() {
        //WebDriverManager.chromedriver().setup();
        WebDriverManager.firefoxdriver().setup();

        try {

            log4j = new Log4j(Hooks.class.getName());
            //driver = new ChromeDriver();
            driver = new FirefoxDriver();
            driver.manage().window().maximize();
            driver.get("https://www.saucedemo.com/");
            log4j.getLogger().info("Opening website https://www.saucedemo.com/");
        } catch (WebDriverException webEx) {
            log4j.getLogger().info("WebDriver Failed: " + webEx.getMessage());
        } catch (Exception ex) {
            log4j.getLogger().fatal(ex.getMessage());
        }
    }

    @After
    public void tearDown() {
        if (driver != null) {
            driver.quit();

        }
    }

}
