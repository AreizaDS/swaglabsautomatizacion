package com.swaglabs.stepdefinitions;

import com.swaglabs.data.User;
import com.swaglabs.data.UserDTO;
import com.swaglabs.pages.LoginPage;
import com.swaglabs.screenshots.DataConfiguration;
import com.swaglabs.screenshots.ScreenShots;
import com.swaglabs.screenshots.SourceImages;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.support.PageFactory;

import java.util.Map;

import static com.swaglabs.stepdefinitions.Hooks.driver;
import static com.swaglabs.stepdefinitions.Hooks.log4j;

public class StepUserLocked {

    //LoginPage loginPage;

    private String suiteEvidenceRoute;

    @Before
    public void setUp() {
        suiteEvidenceRoute = SourceImages.evidenceRoute + "Login\\" + DataConfiguration.getCurrentDate();
    }

    Map<Integer, User> userMap = new UserDTO().getUsers();

    @Given("I want to login in saucedemo webpage")
    public void iWantToLoginInSaucedemoWebpage() {

        driver.get("https://www.saucedemo.com/");
        log4j.getLogger().info("Opening website from StepUserLocked https://www.saucedemo.com/");

    }

    @When("I enters an Know User Locked")
    public void iEntersAnKnowUserLocked() throws Exception {

        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        log4j.getLogger().info("Writing an Username in status locked...");
        loginPage.sendInfoUserName(userMap.get(2).getUsername());
        log4j.getLogger().info("Writing Password...");
        loginPage.sendIfoPassword(userMap.get(2).getPassword());
        log4j.getLogger().info("Clicking on Login button");
        loginPage.clickBtnLogin();
        ScreenShots.takeSnapShot(driver, suiteEvidenceRoute, "MessageUserLocked.png");

    }

    @Then("Its appear message of user locked")
    public void itsAppearMessageOfUserLocked() {

        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        System.out.println(loginPage.validateMessageInvalidUser());
        Assert.assertEquals("Epic sadface: Sorry, this user has been locked out.", loginPage.validateMessageInvalidUser());

    }
}
