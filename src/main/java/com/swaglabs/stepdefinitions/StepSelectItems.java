package com.swaglabs.stepdefinitions;

import com.swaglabs.data.User;
import com.swaglabs.data.UserDTO;
import com.swaglabs.pages.InventoryPage;
import com.swaglabs.pages.LoginPage;
import com.swaglabs.screenshots.DataConfiguration;
import com.swaglabs.screenshots.ScreenShots;
import com.swaglabs.screenshots.SourceImages;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.support.PageFactory;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static com.swaglabs.stepdefinitions.Hooks.driver;

public class StepSelectItems {

    //LoginPage loginPage;

    private String suiteEvidenceRoute;

    @Before
    public void setUp() {
        suiteEvidenceRoute = SourceImages.evidenceRoute + "Inventory\\" + DataConfiguration.getCurrentDate();
    }

    Map<Integer, User> userMap = new UserDTO().getUsers();

    @Given("I want to buy some items in the saucedemo webpage")
    public void iWantToBuySomeItemsInTheSaucedemoWebpage() {

        driver.get("https://www.saucedemo.com/");

    }

    @When("I set the user name and password")
    public void iSetTheUserNameAndPassword() {

        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        loginPage.sendInfoUserName(userMap.get(1).getUsername());
        loginPage.sendIfoPassword(userMap.get(1).getPassword());
        loginPage.clickBtnLogin();

    }

    @And("I choose the desired items that I want to buy")
    public void iChooseTheDesiredItemsThatIWantToBuy() throws Exception {

        InventoryPage inventoryPage = PageFactory.initElements(driver, InventoryPage.class);


        int lenght = inventoryPage.getList().size();
        int productsQuantity = (int)Math.floor(Math.random()*lenght+1);
        System.out.println("cantidad de productos: "+productsQuantity);

        Set<Integer> alreadyUsedNumbers = new HashSet<>();
        for(int i=1; i<(productsQuantity+1);i++){
            int aleatory = (int)(Math.random()*lenght);
            System.out.println("Posicion: "+aleatory);
            if (!alreadyUsedNumbers.contains(aleatory)){
                alreadyUsedNumbers.add(aleatory);
                System.out.println("LLEGAMOS A ESTE PUNTO");
                inventoryPage.clicAddToCart(aleatory);
            }
        }
        inventoryPage.clicBtn();
        ScreenShots.takeSnapShot(driver, suiteEvidenceRoute, "SelectedElements.png");
     }

    @Then("I should view the items in the cart")
    public void iShouldViewTheItemsInTheCart() {

        InventoryPage inventoryPage = PageFactory.initElements(driver, InventoryPage.class);

        Assert.assertEquals("YOUR CART", inventoryPage.validateMessageOnTheCart());
    }
}
