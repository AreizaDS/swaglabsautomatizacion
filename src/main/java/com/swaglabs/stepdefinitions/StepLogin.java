package com.swaglabs.stepdefinitions;

import com.swaglabs.data.User;
import com.swaglabs.data.UserDTO;
import com.swaglabs.pages.LoginPage;
import com.swaglabs.screenshots.DataConfiguration;
import com.swaglabs.screenshots.ScreenShots;
import com.swaglabs.screenshots.SourceImages;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.support.PageFactory;

import java.util.Map;

import static com.swaglabs.stepdefinitions.Hooks.driver;
import static com.swaglabs.stepdefinitions.Hooks.log4j;


public class StepLogin {

    //LoginPage loginPage;

    private String suiteEvidenceRoute;

    @Before
    public void setUp() {
        suiteEvidenceRoute = SourceImages.evidenceRoute + "Login\\" + DataConfiguration.getCurrentDate();
    }


    Map<Integer, User> userMap = new UserDTO().getUsers();

    @Given("I want to access the saucedemo webpage")
    public void iWantToAccessTheSaucedemoWebpage() throws Exception {

        driver.get("https://www.saucedemo.com/");
        ScreenShots.takeSnapShot(driver, suiteEvidenceRoute, "AccessToHome.png");
        log4j.getLogger().info("Opening website from StepLogin https://www.saucedemo.com/");
    }

    @When("I enters the username and password")
    public void iEntersTheUsernameAndPassword() {


        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        log4j.getLogger().info("Writing an Username valid...");
        loginPage.sendInfoUserName(userMap.get(1).getUsername());
        log4j.getLogger().info("Writing Password...");
        loginPage.sendIfoPassword(userMap.get(1).getPassword());
        log4j.getLogger().info("Clicking on Login button");
        loginPage.clickBtnLogin();

    }

    @Then("I should be taken to the homepage")
    public void iShouldBeTakenToTheHomepage() {

        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        System.out.println(loginPage.validateMessageValidUser());
        Assert.assertEquals("PRODUCTS", loginPage.validateMessageValidUser());


    }
}
