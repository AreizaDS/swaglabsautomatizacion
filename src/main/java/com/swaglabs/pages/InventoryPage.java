package com.swaglabs.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;

import java.util.List;

public class InventoryPage {

    WebDriver driver;

    @FindBys(@FindBy(xpath = "//div[@class='pricebar']/button"))
    List<WebElement> addToCart;

    @FindBy(xpath = "//a[@class='shopping_cart_link']")
    WebElement btnCart;

    @FindBy(xpath = "//span[@class='title']")
    WebElement textYourCarText;

    public InventoryPage(WebDriver driver) {
        this.driver = driver;

    }

    public void clicBtn(){
        btnCart.click();

    }
    public void clicAddToCart(int aleatoryNumber){
        addToCart.get(aleatoryNumber).click();

    }
    public String validateMessageOnTheCart(){

        return textYourCarText.getText();
    }
    public List <WebElement> getList(){
        return addToCart;
    }
}

