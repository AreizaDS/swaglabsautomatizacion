package com.swaglabs.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class LoginPage {


    WebDriver driver;

    @FindBy(name = "user-name")
    WebElement userName;

    @FindBy(name = "password")
    WebElement password;

    @FindBy(id = "login-button")
    WebElement btnLogin;

    @FindBy(how = How.XPATH, using = "//*[text() = 'Epic sadface: Sorry, this user has been locked out.']")
    WebElement userLocked;

    @FindBy(xpath = "//span[@class='title']")
    WebElement checkValidText;

    @FindBy(xpath = "//h3[@data-test='error']")
    WebElement checkMessageUserLocked;



    public LoginPage(WebDriver driver) {
        this.driver = driver;
        //PageFactory.initElements(driver, LoginPage.class);
    }

    public void sendInfoUserName(String user) {
        userName.sendKeys(user);
    }

    public void sendIfoPassword(String pass) {
        password.sendKeys(pass);
    }

    public void clickBtnLogin() {
        btnLogin.click();
    }

    public Boolean validateMessageUserLocked() {
        String message = driver.findElement(By.xpath(String.valueOf(userLocked))).getText();
        boolean status;
        if (message.equals("Epic sadface: Sorry, this user has been locked out.")) {
            status = true;
        } else status = false;
        return status;
    }

    public String validateMessageValidUser() {

        return checkValidText.getText();

    }

    public String validateMessageIncorrectCredentials() {

        return checkValidText.getText();

    }

    public String validateMessageInvalidUser() {

        return checkMessageUserLocked.getText();

    }

    public void loginSuccess(String userName, String password) {
        sendInfoUserName(userName);
        sendIfoPassword(password);
        clickBtnLogin();
    }
}
