package com.swaglabs.data;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class UserDTO {
    private final Connection connection;

    public UserDTO() {
        this.connection = new MySQLConnection("localhost", "3306", "swag_labs",
                "root", "").getMySQLConnection();
    }

    public Map<Integer, User> getUsers() {

        Map<Integer, User> userMap = new HashMap<>();

        try {
            String sqlQuery = "Select * From user_list";

            ResultSet resultSet = connection.createStatement().executeQuery(sqlQuery);

            while (resultSet.next()) {
                Integer id = resultSet.getInt("ID");
                String username = resultSet.getString("user_name");
                String password = resultSet.getString("password");

                User user = new User(id, username, password);

                userMap.put(id, user);
            }

        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }

        return userMap;
    }
}
