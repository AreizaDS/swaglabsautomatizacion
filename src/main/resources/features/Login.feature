Feature: User Login
  As a potential customer
  I want to insert the login credentials
  To access the platform

  Scenario: successful login
    Given I want to access the saucedemo webpage
    When I enters the username and password
    Then I should be taken to the homepage


  Scenario: Select Items to buy
    Given I want to buy some items in the saucedemo webpage
    When I set the user name and password
    And I choose the desired items that I want to buy
    Then I should view the items in the cart

  Scenario: User Locked
    Given I want to login in saucedemo webpage
    When I enters an Know User Locked
    Then Its appear message of user locked

  Scenario: Wrong Credentials
    Given I want to login in webpage named saucedemo
    When I enters an invalid user and password
    Then Its appear message of username and password are invalid


